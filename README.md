

# Demo Use-Case OpenSenseMap

## Projektbeschreibung

In diesem Use-Case wurde die Open Data API von [OpenSensemapMap](https://openweathermap.org/) eingebunden. Die API liefert diverse Sensormesswerte zu Wetter und Luftqualität, die von Community-betriebenen SenseBoxen erfasst werden.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installation

Für die Installation und Konfiguration dieses Use-Cases wird auf das [deployment repository](https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/deployment) verwiesen

## Funktionsweise

Der NodeRED-Flow ruft die Daten aller Stationen in einem konfigurierten Bereich per HTTP-Request von der API ab. Diese Daten werden aggregiert und an die Datenplattform gesendet. Da es für die gelieferten Daten kein geeignetes NGSI-Schema gibt, wird ein nicht-standardisiertes Schema `SenseBoxObserved` verwendet.


## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/air_quality_osm